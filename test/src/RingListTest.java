package src;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Nodo;
import model.data_structures.RingList;
import model.vo.VORoute;
import model.vo.VOStop;

public class RingListTest {

	private IList<VOStop> listaStops;
	private VOStop ruta1;
	private VOStop ruta2;
	private VOStop ruta3;
	private VOStop ruta4;


	private Nodo<VOStop> nodo1;
	private Nodo<VOStop> nodo2;
	private Nodo<VOStop> nodo3;
	private Nodo<VOStop> nodo4;



	@Test
	public void test() {
		testAgregarLista();
		testDelete();
		testIterator();
	}




	public void setupEscenario1()
	{

		listaStops = new RingList<>();

		ruta1 = new VOStop(1, "", "Prado", "", 4, 4, "Norte", "url", "Cerca", 0);
		ruta2 = new VOStop(11, "", "Calle 26", "", 12, 334, "Sur", "url2", "Locaci�n", 0);
		ruta3 = new VOStop(3, "", "Portal 20", "", 1, 43, "saoe", "url2", "lejos", 0);
		ruta4 = new VOStop(2, "", "Virrey", "", 5, 2, "Norte", "url3", "medio lejos", 0); 


		nodo1 = new Nodo<VOStop>(ruta1, null, null);
		nodo2 = new Nodo<VOStop>(ruta2, null, null);
		nodo3 = new Nodo<VOStop>(ruta3, null, null);
		nodo4 = new Nodo<VOStop>(ruta4, null, null);

		listaStops.add(nodo1);
		listaStops.add(nodo2);
		listaStops.add(nodo3);
		listaStops.addAtEnd(nodo4);

	}




	public void setupEscenario2()
	{
		listaStops = new RingList<>();

		ruta1 = new VOStop(1, "", "Prado", "", 4, 4, "Norte", "url", "Cerca", 0);
		ruta2 = new VOStop(11, "", "Calle 26", "", 12, 334, "Sur", "url2", "Locaci�n", 0);
		ruta3 = new VOStop(3, "", "Portal 20", "", 1, 43, "saoe", "url2", "lejos", 0);
		ruta4 = new VOStop(2, "", "Virrey", "", 5, 2, "Norte", "url3", "medio lejos", 0); 


		nodo1 = new Nodo<VOStop>(ruta1, null, null);
		nodo2 = new Nodo<VOStop>(ruta2, null, null);
		nodo3 = new Nodo<VOStop>(ruta3, null, null);
		nodo4 = new Nodo<VOStop>(ruta4, null, null);

		listaStops.addAtEnd(nodo1);
		listaStops.addAtEnd(nodo2);
		listaStops.addAtEnd(nodo3);
		listaStops.addAtEnd(nodo4);


	}


	public void setupEscenario3()
	{
		
		listaStops = new DoubleLinkedList<>();
		
		ruta1 = new VOStop(1, "", "Prado", "", 4, 4, "Norte", "url", "Cerca", 0);
		ruta2 = new VOStop(11, "", "Calle 26", "", 12, 334, "Sur", "url2", "Locaci�n", 0);
		ruta3 = new VOStop(3, "", "Portal 20", "", 1, 43, "saoe", "url2", "lejos", 0);
		ruta4 = new VOStop(2, "", "Virrey", "", 5, 2, "Norte", "url3", "medio lejos", 0); 
		
		
		nodo1 = new Nodo<VOStop>(ruta1, null, null);
		nodo2 = new Nodo<VOStop>(ruta2, null, null);
		nodo3 = new Nodo<VOStop>(ruta3, null, null);
		nodo4 = new Nodo<VOStop>(ruta4, null, null);
		
		listaStops.addAtk(nodo1, 0);
		listaStops.addAtk(nodo2, 1);
		listaStops.addAtk(nodo3, 1);
		listaStops.addAtk(nodo4, 2);
		
	}
	

	public void testAgregarLista()
	{

		setupEscenario1();



		assertTrue("El numero no es el esperado" , listaStops.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaStops.getElement(0).equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(2).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(3).equals(nodo4.darElemento()));


		setupEscenario2();


		assertTrue("El numero no es el esperado" , listaStops.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaStops.getElement(0).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(1).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(2).equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(3).equals(nodo4.darElemento()));

		
		setupEscenario3();
		assertTrue("El numero no es el esperado" , listaStops.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaStops.getElement(0).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(1).equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(3).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(2).equals(nodo4.darElemento()));

	}
	
	
	
	public void testDelete()
	{
		
		setupEscenario1();
		
		
		listaStops.delete(); 
		
		assertTrue("El numero no es el esperado", listaStops.getSize() == 3);
		assertTrue("El primer elemento no es el esperado", listaStops.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(1).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(2).equals(nodo4.darElemento()));
		
		listaStops.deleteAtk(2);
		
		assertTrue("El numero no es el esperado", listaStops.getSize() == 2);
		assertTrue("El primer elemento no es el esperado", listaStops.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(1).equals(nodo1.darElemento()));

		listaStops.delete();
		assertTrue("El numero no es el esperado", listaStops.getSize() == 1);
		assertTrue("El elemento no es el esperado", listaStops.getElement(0).equals(nodo1.darElemento()));
		
		setupEscenario1();
		
		listaStops.deleteAtk(0);
		assertTrue("El elemento no es el esperado", listaStops.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(1).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(2).equals(nodo4.darElemento()));
		
		
		listaStops.deleteAtk(1);
		assertTrue("El numero no es el esperado", listaStops.getSize() == 2);
		assertTrue("El elemento no es el esperado", listaStops.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaStops.getElement(1).equals(nodo4.darElemento()));
		
	}
	
	public void testIterator()
	{
		
		setupEscenario2();
		
		Iterator<VOStop> iter = listaStops.iterator(); 
		
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo4.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next()==null);
		assertFalse(iter.hasNext());
		
	}
	
	
	
	
	

}
