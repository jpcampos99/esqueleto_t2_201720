package src;
import static org.junit.Assert.*;

import java.util.Iterator;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Nodo;
import model.vo.VORoute;

import org.junit.Test;

import com.sun.corba.se.impl.orb.ParserTable.TestIIOPPrimaryToContactInfo;




public class DoubleLinkedListTest<T> {

	
	private IList<VORoute> listaRutas;
	private VORoute ruta1;
	private VORoute ruta2;
	private VORoute ruta3;
	private VORoute ruta4;
	
	
	private Nodo<VORoute> nodo1;
	private Nodo<VORoute> nodo2;
	private Nodo<VORoute> nodo3;
	private Nodo<VORoute> nodo4;
	
	@Test
	public void test() {
		testAgregarLista();
		testDelete();
		testIterator();
	}

	public void setupEscenario1()
	{
		
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new VORoute(1, "Agencia", "k23", "PortalEldorado", "", 1, "url", "", "");
		ruta2 = new VORoute(2, "Agencia", "j24", "Universidades", "", 1, "url2", "", "");
		ruta3 = new VORoute(3, "Agencia", "b74", "PortalNorte", "", 1, "url3", "", "");
		ruta4 = new VORoute(4, "Agencia", "d70", "Portal80", "", 1, "url4", "", "");
		
		
		nodo1 = new Nodo<VORoute>(ruta1, null, null);
		nodo2 = new Nodo<VORoute>(ruta2, null, null);
		nodo3 = new Nodo<VORoute>(ruta3, null, null);
		nodo4 = new Nodo<VORoute>(ruta4, null, null);
		
		listaRutas.add(nodo1);
		listaRutas.add(nodo2);
		listaRutas.add(nodo3);
		listaRutas.addAtEnd(nodo4);
		
	}
	
	public void setupEscenario2()
	{
		
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new VORoute(1, "Transmilenio", "k23", "PortalEldorado", "", 1, "url", "", "");
		ruta2 = new VORoute(2, "Transmilenio", "j24", "Universidades", "", 1, "url2", "", "");
		ruta3 = new VORoute(3, "Transmilenio", "b74", "PortalNorte", "", 1, "url3", "", "");
		ruta4 = new VORoute(4, "Transmilenio", "d70", "Portal80", "", 1, "url4", "", "");
		
		
		nodo1 = new Nodo<VORoute>(ruta1, null, null);
		nodo2 = new Nodo<VORoute>(ruta2, null, null);
		nodo3 = new Nodo<VORoute>(ruta3, null, null);
		nodo4 = new Nodo<VORoute>(ruta4, null, null);
		
		listaRutas.addAtEnd(nodo1);
		listaRutas.addAtEnd(nodo2);
		listaRutas.addAtEnd(nodo3);
		listaRutas.addAtEnd(nodo4);
		
	}
	
	
	public void setupEscenario3()
	{
		
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new VORoute(1, "Transmilenio", "k23", "PortalEldorado", "", 1, "url", "", "");
		ruta2 = new VORoute(2, "Transmilenio", "j24", "Universidades", "", 1, "url2", "", "");
		ruta3 = new VORoute(3, "Transmilenio", "b74", "PortalNorte", "", 1, "url3", "", "");
		ruta4 = new VORoute(4, "Transmilenio", "d70", "Portal80", "", 1, "url4", "", "");
		
		
		nodo1 = new Nodo<VORoute>(ruta1, null, null);
		nodo2 = new Nodo<VORoute>(ruta2, null, null);
		nodo3 = new Nodo<VORoute>(ruta3, null, null);
		nodo4 = new Nodo<VORoute>(ruta4, null, null);
		
		listaRutas.addAtk(nodo1, 0);
		listaRutas.addAtk(nodo2, 1);
		listaRutas.addAtk(nodo3, 1);
		listaRutas.addAtk(nodo4, 2);
		
	}
	
	
	public void testAgregarLista()
	{
		
		setupEscenario1();
		
	
	
		assertTrue("El numero no es el esperado" , listaRutas.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(3).equals(nodo4.darElemento()));

		
		setupEscenario2();
		
		
		assertTrue("El numero no es el esperado" , listaRutas.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(3).equals(nodo4.darElemento()));
		
		
		setupEscenario3();

		assertTrue("El numero no es el esperado" , listaRutas.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(3).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(nodo4.darElemento()));
	}
	
	
	
	public void testDelete()
	{
		
		setupEscenario1();
		
		
		listaRutas.delete(); 
		
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 3);
		assertTrue("El primer elemento no es el esperado", listaRutas.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(nodo4.darElemento()));
		
		listaRutas.deleteAtk(2);
		
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 2);
		assertTrue("El primer elemento no es el esperado", listaRutas.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(nodo1.darElemento()));

		listaRutas.delete();
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 1);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(nodo1.darElemento()));
		
		setupEscenario1();
		
		listaRutas.deleteAtk(0);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(nodo4.darElemento()));
		
		
		listaRutas.deleteAtk(1);
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 2);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(nodo4.darElemento()));
		
	}
	
	
	
	public void testIterator()
	{
		
		setupEscenario2();
		
		Iterator<VORoute> iter = listaRutas.iterator(); 
		
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo1.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo2.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo3.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next().equals(nodo4.darElemento()));
		assertTrue("El elemento no es el esperado", iter.next()==null);
		assertFalse(iter.hasNext());
		
	}
	
	
	
	
}
