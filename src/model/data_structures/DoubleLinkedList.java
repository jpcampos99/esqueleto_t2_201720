package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList <T> implements IList<T>
{

	//Las	 listas	
	//doblemente	encadenadas	deben	tener	una	referencia	a	su	primer	y	�ltimo	
	//elemento,	y	cada nodo	en	la	lista	debe	poder	acceder	a	su	sucesor	y	
	//predecesor	(excepto	en	el	caso	del	primer	y	�ltimo	elemento	de	la	lista).


	private Nodo<T> cabeza; 
	private Nodo<T> cola; 
	private int size = 0; 

	public DoubleLinkedList( )
	{
		cabeza = new Nodo<T>(null, null, null); 
		cola = new Nodo<T>(null, cabeza, null); 
		cabeza.cambiarSiguiente(cola);		
	}

	public boolean estaVacio( )
	{
		return size == 0; 
	}

	public T primero( )
	{
		if( estaVacio() )
		{
			return null; 
		}
		else
		{
			return cabeza.darElemento(); 
		}	
	}

	public T ultimo( )
	{
		if( estaVacio() )
		{
			return null; 
		}
		else
		{
			return cola.darElemento(); 
		}
	}


	@Override
	public Integer getSize() {

		return size;
	}

	
	@Override
	public Iterator<T> iterator() {

		Iterator<T> iter = new Iterator<T>() {

			private int currentIndex = 0;

			@Override
			public boolean hasNext() {
				return currentIndex < size && getElement(currentIndex)!= null;

			}

			@Override
			public T next() {
				
				return getElement(currentIndex++);
			}

		};
		return iter;



	}

	
	@Override//Agrega al principio
	public void add(Nodo<T> n) {

		if(estaVacio())
		{
			cabeza = n;
			cola = n;
			size++;
		}
		else 
		{
			Nodo<T> viejoPrimero = cabeza;

			cabeza = n;

			cabeza.cambiarSiguiente(viejoPrimero);

			viejoPrimero.cambiarAnterior(cabeza);
			size++;
		}

	}

	
	@Override
	public void addAtEnd(Nodo<T> n) {
		
		if( estaVacio() )
		{
			cabeza = n; 
			cola = n;
			size++;
		}
		else
		{
			
			Nodo<T> viejaCola = cola;
			
			
			cola = n;
			n.cambiarAnterior(viejaCola);
			viejaCola.cambiarSiguiente(n);
			size++;
		}
	}

	
	@Override
	public void addAtk(Nodo<T> n, int k) {


		
		
		if(k > size + 1)
		{
			System.out.println("No se puede agregar we");
			return;
		}
		
		if(estaVacio())
		{
			
			cabeza = n; 
			cola = n;
			size++;
		}
		
		else if(k == 0)//Si agrega en la primera posici�n
		{
			add(n);
		}
		
		else if(k == size)//si agrega en la �ltima posici�n
		{
			addAtEnd(n);
		}
		
		else
		{
			Nodo<T> nodoDespues = getNodo(k);
			Nodo<T> nodoAntes = nodoDespues.darAnterior();


			n.cambiarSiguiente(nodoDespues);
			n.cambiarAnterior(nodoAntes);
			
			nodoDespues.cambiarAnterior(n);
			nodoAntes.cambiarSiguiente(n);
			
			size++;
		}


	}

	@Override
	public T getElement(int k) {


		if(estaVacio()|| k > size -1 )
		{
			return null;
		}

		else
		{

			int i = 0;
			Nodo<T> nodo = cabeza;

			while( nodo.siguiente != null)
			{

				if(i == k)
				{
					return nodo.darElemento();
				}
				else{
					nodo = nodo.darSiguiente();
					i++;
				}


			}

			return nodo.darElemento();
		}
	}



	public Nodo<T> getNodo(int k) {


		if(estaVacio()	)
		{
			return null;
		}
		else
		{

			int i = 0;
			Nodo<T> nodo = cabeza;

			while( nodo.siguiente != null)
			{

				if(i == k)
				{
					return nodo;
				}
				else{
					nodo = nodo.darSiguiente();
					i++;
				}


			}

			return nodo;
		}
	}

	@Override
	public Nodo<T> getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}



	//elimina el primero
	@Override
	public void delete() {


		if(estaVacio())
		{
			System.out.println("No hay elementos para eliminar");
			return;
		}

		Nodo <T> nuevoPrimero = cabeza.darSiguiente();

		
		cabeza = nuevoPrimero;
		nuevoPrimero.cambiarAnterior(null);
		size--;


	}



	@Override
	public void deleteAtk(int k) {

		if(estaVacio())
		{
			System.out.println("No hay elementos para eliminar");
			return;
		}

		
		Nodo<T> nodoAEliminar = getNodo(k);
		
		if(nodoAEliminar == cola)
		{
			Nodo<T> nodoAnterior = nodoAEliminar.darAnterior();
			nodoAnterior.cambiarSiguiente(null);
			cola = nodoAnterior;
			size--;
		}
		else if (nodoAEliminar == cabeza)
		{
			Nodo<T> nodoSiguiente = nodoAEliminar.darSiguiente();
			nodoAEliminar.cambiarSiguiente(null);
			cabeza = nodoSiguiente; 
			size--;
		}
		
		else
		{
			
		Nodo<T> nodoAnterior = nodoAEliminar.darAnterior();
		Nodo<T> nodoSiguiente = nodoAEliminar.darSiguiente();


		nodoAnterior.cambiarSiguiente(nodoSiguiente);
		nodoSiguiente.cambiarAnterior(nodoAnterior);

		nodoAEliminar.cambiarAnterior(null);
		nodoAEliminar.cambiarSiguiente(null);
		size--;
		}
		
		
	}



}