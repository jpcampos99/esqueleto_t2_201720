package model.data_structures;

import java.util.Iterator;

public class RingList <T> implements IList<T>
{
	//La	lista	circular	debe	tener	una	referencia	al	primer	
	//elemento	de	la lista,	 donde	 cada	 nodo	 puede	 acceder	 
	//a	 su	 sucesor	 y	 predecesor.	 	 Ademas,	 el	 acceso	 a	 los	
	//elementos	de	la	lista	circular	debe	respetar	las	reglas	de	la
	//aritmetica	modular,	utilizando	como 	modulo	 el	 numero	 de	 
	//elementos	 de	 la	 lista.	

	private Nodo<T> cabeza; 

	private Nodo<T> cola; 

	private int size = 0; 

	public RingList( )
	{
		cabeza = new Nodo<T>(null, null, null); 
		cola = new Nodo<T>(null, cabeza, null); 
		cabeza.cambiarSiguiente(cola);		
	}

	public boolean estaVacio( )
	{
		return size == 0; 
	}


	public T primero( )
	{
		if( estaVacio() )
		{
			return null; 
		}
		else
		{
			return cabeza.darElemento(); 
		}	
	}

	public T ultimo( )
	{
		if( estaVacio() )
		{
			return null; 
		}
		else
		{
			return cola.darElemento(); 
		}
	}

	public Integer getSize()
	{
		return size;
	}


	public Iterator<T> iterator() {

		Iterator<T> iter = new Iterator<T>() {

			private int currentIndex = 0;

			public boolean hasNext() {
				return currentIndex < size && getElement(currentIndex)!= null;

			}

			public T next() {

				return getElement(currentIndex++);
			}

		};
		return iter;
	}

	public void add( Nodo<T> n) 
	{
		if (estaVacio() ) 
		{
			cabeza = n;
			cola = n;  
			cola.cambiarSiguiente(n);
		}
		else
		{
			n.cambiarSiguiente(cabeza);
			cabeza = n; 
			cola.cambiarSiguiente(cabeza);
		}

		size++; 
	}

	public void addAtEnd( Nodo<T> n) 
	{
		if (estaVacio() ) 
		{
			cabeza = n;
			cola = n;
			cola.cambiarSiguiente(n);
		}
		else
		{
			cola.cambiarSiguiente(n);
			n.cambiarSiguiente(n);
			cola = n;
		}

		size++; 
	}


	public void addAtk(Nodo<T> n, int k) 
	{
       if( k >= 0 && k <= size )
       {
           Nodo<T>nodo = n; 
           
           if(k == 0)
           {
           	nodo.cambiarSiguiente(cabeza);
               cabeza = nodo;
               cola.cambiarSiguiente(cabeza);
           }
           else
           {
               if(k == size)
               {
               	cola.cambiarSiguiente(nodo);
               	nodo.cambiarSiguiente(cabeza);
                cola = nodo;     
               }
               else
               {

                   Nodo<T> nodo1 = cabeza;

                   for (int i = 0; i < (k-1); i++) 
                   {
                   	nodo1 = nodo1.darSiguiente();
                   }

                   Nodo<T> siguiente = nodo1.darSiguiente();
                   nodo1.cambiarSiguiente(nodo);
                   nodo.cambiarSiguiente(siguiente);
               }
           }

           size++;
       }

	}

	public T getElement(int k) 
	{
		T elemento = null; 

		if(k >= 0 && k < size)
		{
			if (k == 0) 
			{
				elemento = cabeza.darElemento(); 
			}
			else
			{
				Nodo<T> nodo = cabeza;

				for (int i = 0; i < k; i++) 
				{
					nodo = nodo.darSiguiente(); 
				}

				elemento = (T) nodo.darElemento(); 
			}
		}

		return elemento; 
		// TODO Auto-generated method stub
	}

	public Nodo<T> getNodo(int k)
	{
		if(estaVacio())
		{
			return null;
		}
		else
		{
			int i = 0;
			Nodo<T> nodo = cabeza;

			while( nodo.siguiente != null)
			{
				if(i == k)
				{
					return nodo;
				}
				else{
					nodo = nodo.darSiguiente();
					i++;
				}
			}

			return nodo;
		}
	}

	public void delete() 
	{
		Nodo <T> nodo = cabeza.darSiguiente();

		cabeza = nodo;
		nodo.cambiarAnterior(null);
		size--;
	}

	public void deleteAtk(int k) 
	{
		if(k >= 0 && k < size)
		{
			if( k == 0)
			{
				cabeza = cabeza.darSiguiente();
				cola.cambiarSiguiente(cabeza);
			}
			else
			{
				Nodo<T> nodo = cabeza;

				for (int i = 0; i < k-1; i++) 
				{
					nodo = nodo.darSiguiente();
				}

				if (nodo.darSiguiente() == cola) 
				{
					nodo.cambiarSiguiente(cabeza);
					cola = nodo;
				} 
				else 
				{
					Nodo<T> siguiente = nodo.darSiguiente();
					nodo.cambiarSiguiente(siguiente.darSiguiente());  
				}
			}

			size--;
		}
		// TODO Auto-generated method stub
	}

	@Override
	public Nodo<T> getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}

}
