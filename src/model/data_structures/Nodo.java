package model.data_structures;

public class Nodo <T>
{
	
	T elemento;
	
	Nodo <T>anterior;
	
	Nodo <T> siguiente; 	
	
	public Nodo( T element, Nodo<T> anteri, Nodo<T> siguien)
	{
		elemento = element;
		anterior = anteri; 
		siguiente = siguien; 	
	}
	
	public T darElemento( )
	{
		return elemento; 
	}
	
	public Nodo<T> darAnterior()
	{
		return anterior; 	
	}
	
	public Nodo<T> darSiguiente( )
	{
		return siguiente; 
	}
	
	public void cambiarSiguiente( Nodo<T> pSig)
	{
		siguiente = pSig; 
	}
	
	public void cambiarAnterior( Nodo<T> pAnt)
	{
		anterior = pAnt; 
	}
	
}