package model.data_structures;

import java.util.Iterator;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */

//Tecnicamente es una cola

public interface IList<T> extends Iterable<T> {

	//Agrega al principio
	public void add(Nodo<T> n);
	
	//Agrega al final
	public void addAtEnd(Nodo<T> n);
	
	//Agrega en posici�n k
	public void addAtk(Nodo <T> n, int k);
	
	//Da el elemento en posici�n k
	public T getElement(int k);
	
	//No se que hace esto
	public Nodo <T> getCurrentElement();
	
	//Da el iterador
	public Iterator<T> iterator();
	
	//Elimina el primero
	public void delete();
	
	//Elimina en posici�n k
	public void deleteAtk(int k);
	
	
	Integer getSize();

}
