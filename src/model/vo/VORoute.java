package model.vo;

import model.data_structures.IList;
import model.data_structures.Nodo;
import model.data_structures.RingList;

/**
 * Representation of a route object
 */
public class VORoute {

	private int routeId;
	private String agencyId;
	private String routeShortName;
	private String routeLongName;
	private String routeDesc; 
	private int routeType;
	private String routeUrl;
	private String routeColor; 	
	private String routeTextColor;

	private int min;
	
	
	private IList<VOTrip> listaTrips;
	
	
	
	public VORoute(int pRouteId, String pAgencyId, String pRouteShortName,String pRouteLongName, String pRouteDesc, int pRouteType, String pRouteUrl, String pRouteColor, String pRouteTextColor)
	{
		listaTrips = new RingList<>();
		
		routeId = pRouteId;
		setAgencyId(pAgencyId); 
		setRouteShortName(pRouteShortName);
		routeLongName = pRouteLongName;
		routeDesc = pRouteDesc;
		routeType = pRouteType;
		routeUrl = pRouteUrl;
		routeColor = pRouteColor;
		routeTextColor = pRouteColor;
		
		
	}
	
	
	
	public void addARing(Nodo<VOTrip> n)
	{
		for (int i = 0; i < listaTrips.getSize(); i++) {
	      
	        if (listaTrips.getElement(i).getTripId() < n.darElemento().getTripId()) continue;
	        
	       listaTrips.addAtk(n, i);
	        return;
	    }
	
	    listaTrips.addAtEnd(n);
	} 	
		
		
	
	
	public IList<VOTrip> getListaTrips()
	{
		return listaTrips; 
	}
	
	
	/**
	 * @return id - Route's id number
	 */
	public int getId() {
		// TODO Auto-generated method stub
		return routeId;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return routeLongName;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getRouteShortName() {
		return routeShortName;
	}

	public void setRouteShortName(String routeShortName) {
		this.routeShortName = routeShortName;
	}

}
