package model.vo;

import javax.crypto.interfaces.PBEKey;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Nodo;

public class VOTrip {

	private int routeId;
	private int serviceId;
	private int tripId;
	private String tripHeadsign;
	private String tripShortName;
	private int directionId;
	private int shapeId;
	private boolean wheelchairAccesible;
	private boolean bikesAllowed;
	
	private IList<VOStopTimes> listaStopTimes;
	
	

	public VOTrip(int prouteId, int pserviceId, int ptripId, String ptripHeadsing, String ptripShortName, int pdirectionId, int pshapeId, boolean pwheelchairaccesible, boolean pbikes ) {
		
		listaStopTimes = new DoubleLinkedList<>();
		
		setRouteId(prouteId);
		serviceId = pserviceId;
		setTripId(ptripId); 
		tripHeadsign = ptripHeadsing;
		setTripShortName(ptripShortName);
		directionId = pdirectionId; 
		shapeId = pshapeId; 
		wheelchairAccesible = pwheelchairaccesible; 
		bikesAllowed = pbikes;

		
	}


	public IList<VOStopTimes> getListaStopTimes(){
		
		return listaStopTimes;
	}
	
	public void addAList(Nodo<VOStopTimes> n )
	{
		listaStopTimes.add(n);
	}

	public int getRouteId() {
		return routeId;
	}



	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}



	public int getTripId() {
		return tripId;
	}



	public void setTripId(int tripId) {
		this.tripId = tripId;
	}



	public String getTripShortName() {
		return tripShortName;
	}



	public void setTripShortName(String tripShortName) {
		this.tripShortName = tripShortName;
	}
	
	
	public String toString()
	{
		return "" +  routeId; 
	}
	
	
	
	
}
