package model.vo;



/**
 * Representation of a Stop object
 */
public class VOStop {

	
	private int stopId;
	private String stopCode;
	private String stopName; 	
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String zoneId;
	private String stopUrl;
	private String locationType;
	private int parentStation;
	
	
	public VOStop(int pStopId, String pStopCode, String pStopName, String pStopDesc, double pStopLat, double pStopLon, String pZoneId, String pStopUrl, String pLocationType, int pPartenStation)
	
	{
		
		stopId = pStopId;
		stopCode = pStopCode;
		stopName = pStopName;
		stopDesc = pStopDesc;
		stopLat = pStopLat;
		stopLon = pStopLon; 
		setZoneId(pZoneId); 
		locationType = pLocationType; 
		parentStation = pPartenStation;
		
	}
	
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return stopId;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return stopName;
	}


	public String getZoneId() {
		return zoneId;
	}


	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	
	  public int compararPorNombre( VOStop stop )
	    {
	        int valorDeComparación = zoneId.compareTo( stop.zoneId );
	        
	        if(valorDeComparación < 0)
	        {
	            return -1; 
	        }
	                
	        if(valorDeComparación == 0)
	        {
	            return 0;
	        }
	        
	        if(valorDeComparación > 0)
	        {
	            return 1;
	        }
	        
	        return 2;
	        
	        
	    }
	
	
}
