package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import com.sun.corba.se.impl.orbutil.graph.Node;
import com.sun.org.apache.bcel.internal.generic.GETFIELD;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Nodo;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	IList<VORoute> listaRutas;
	IList<VOStop> listaParadas; 
	IList<VOStopTimes> listaStoptimes;
	
	
	
	@Override

	public void loadRoutes(String routesFile) {

		BufferedReader lector = null;
		listaRutas = new DoubleLinkedList<>();

		try{
			lector = new BufferedReader(new FileReader(routesFile));
			String linea = lector.readLine();
			linea = lector.readLine();



			while(linea!= null)
			{
				String[] partes = linea.split(",");






				int route_id = Integer.parseInt(partes[0]);
				String agency_id = partes[1];
				String route_short_name = partes[2];
				String route_long_name = partes[3].trim();
				String route_desc = partes[4];
				int route_type = Integer.parseInt(partes[5]); 
				String route_url = partes[6];
				String route_color = partes[7];
				String 	route_text_color = partes[8];     

				VORoute ruta = new VORoute(route_id, agency_id, route_short_name, route_long_name, route_desc, route_type, route_url, route_color, route_text_color);
				Nodo<VORoute> nodo = new Nodo<>(ruta, null, null); 


				listaRutas.addAtEnd(nodo);


				linea = lector.readLine();
			}



		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			try {
				lector.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void loadTrips(String tripsFile) {

		BufferedReader lector = null;

		int cuantosAgrego = 0;
		try{
			lector = new BufferedReader(new FileReader(tripsFile));
			String linea = lector.readLine();
			linea = lector.readLine();

			if(listaRutas == null){

				System.out.println("Primero cargue la lista de rutas por fa");
				return; 
			}



			while(linea!= null)
			{
				String[] partes = linea.split(",");


				int route_id = Integer.parseInt(partes[0]);
				int service_id= Integer.parseInt(partes[1]);
				int trip_id = Integer.parseInt(partes[2]);
				String trip_headsign = partes[3];
				String trip_short_name = partes[4];
				int direction_id = Integer.parseInt(partes[5]); 
				int block_id = Integer.parseInt(partes[6]);
				int	shape_id = Integer.parseInt(partes[7]);
				boolean wheelchair_accesible = partes[8].equals("0") ? false : true; 
				boolean bikes_allowed = partes[9].equals("0") ? false : true; 


				linea = lector.readLine();

				VOTrip trip = new VOTrip(route_id, service_id, trip_id, trip_headsign, trip_short_name, direction_id, shape_id, wheelchair_accesible, bikes_allowed);
				Nodo<VOTrip> nodo = new Nodo<VOTrip>(trip, null, null);


				for (int i = 0; i < listaRutas.getSize(); i++) {

					VORoute ruta = listaRutas.getElement(i);

					if(ruta.getId() == route_id)
					{
						ruta.addARing(nodo);
						cuantosAgrego++;
					}

				}


			}

			System.out.println("agrego estos trips: " + cuantosAgrego);

		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			if(lector != null)
			{
				try {
					lector.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}



		// TODO Auto-generated method stub

	}

	@Override
	public void loadStopTimes(String stopTimesFile) {


		BufferedReader lector = null;

		int cuantosAgrego = 0;

		if(listaRutas == null){

			System.out.println("Primero cargue la lista de rutas por fa");
			return; 
		}

		else if(listaRutas.getElement(0).getListaTrips().getSize() == 0)
		{
			System.out.println("Primero cargue la lista trips por fa");
			return;
		}




		listaStoptimes = new DoubleLinkedList<>();

		try{
			lector = new BufferedReader(new FileReader(stopTimesFile));
			String linea = lector.readLine();
			linea = lector.readLine();
			while(linea!= null)
			{
				String[] partes = linea.split(",");

				int trip_id = Integer.parseInt(partes[0]); 
				String arrival_time = partes[1];
				String departure_time = partes[2];
				int stop_id = Integer.parseInt(partes[3]); 
				int stop_sequence = Integer.parseInt(partes[4]);
				String stop_headsign = partes[5];
				int pickup_type = Integer.parseInt(partes[6]);
				int drop_off_type = Integer.parseInt(partes[7]);

				double shape_dist_traveled = 0;

				if(partes.length > 8)
				{
					shape_dist_traveled = Double.parseDouble(partes[8]); 
				}

				VOStopTimes stopTime = new VOStopTimes(trip_id, arrival_time, departure_time, stop_id, stop_sequence, stop_headsign, pickup_type, drop_off_type, shape_dist_traveled);
				Nodo<VOStopTimes> nodo = new Nodo<VOStopTimes>(stopTime, null, null);

				listaStoptimes.add(nodo);
				
				/*
				for (int i = 0; i < listaRutas.getSize(); i++) {
					
					VORoute ruta = listaRutas.getElement(i);
					IList<VOTrip> listaTrips = ruta.getListaTrips();
					
					for (int j = 0; j < listaTrips.getSize(); j++) {
						
						VOTrip trip = listaTrips.getElement(j);
						
						if(trip.getTripId() == trip_id)
						{
							trip.addAList(nodo);
							System.out.println(cuantosAgrego++);
						}
						
					}
					
				}
				*/
				


				linea = lector.readLine();
			}
			System.out.println("agrego " + listaStoptimes.getSize());

		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			if(lector != null)
			{
				try {
					lector.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}



		// TODO Auto-generated method stub

	}

	@Override
	public void loadStops(String stopsFile) {

		BufferedReader lector = null;
		listaParadas = new RingList<>();



		try{
			lector = new BufferedReader(new FileReader(stopsFile));
			String linea = lector.readLine();
			linea = lector.readLine();
			while(linea!= null)
			{


				String[] partes = linea.split(",");

				int stop_id = Integer.parseInt(partes[0]);
				String stop_code = partes[1];
				String stop_name = partes[2];
				String stop_desc = partes[3]; 
				double stop_lat = Double.parseDouble(partes[4]);
				double stop_lon = Double.parseDouble(partes[5]); 
				String zone_id = partes[6];
				String stop_url = partes[7];
				String location_type = partes [8];
				int parent_station = Integer.parseInt(partes[8]);

				VOStop stop = new VOStop(stop_id, stop_code, stop_name, stop_desc, stop_lat, stop_lon, zone_id, stop_url, location_type, parent_station);
				Nodo <VOStop> nodo = new Nodo<VOStop>(stop, null, null);

				agregarAListaStops(nodo);



				linea = lector.readLine();
			}

		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			if(lector != null)
			{
				try {
					lector.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}




		// TODO Auto-generated method stub

	}


	public void agregarAListaStops (Nodo<VOStop> nodo)
	{
		if(listaParadas.getSize() == 0)
		{
			listaParadas.add(nodo);
		}
		else {
			for (int i = 0; i < listaParadas.getSize(); i++) {

				if (listaParadas.getElement(i).compararPorNombre(nodo.darElemento()) == -1) continue;

				listaParadas.addAtk(nodo, i);
				return;
			}	

			listaParadas.addAtEnd(nodo);
		}


	}



	@Override
	public IList<VORoute> routeAtStop(String stopName) {

		System.out.println("Entra al metodo route at stop");

		IList<VORoute> rutasEnParadas = new DoubleLinkedList<>();

		int stopId = 0;


		for (int l = 0; l < listaParadas.getSize(); l++) {

			VOStop parada = listaParadas.getElement(l);

			if("DUNBAR LOOP".equals(parada.getName()))
			{
				stopId = parada.id();
				break;
			}
		}

		System.out.println("Modifica el id" + stopId);


		for (int i = 0; i < listaRutas.getSize(); i++) {

			VORoute ruta = listaRutas.getElement(i);
			System.out.println("id ruta: "+ ruta.getId());

			IList<VOTrip> listaTrips = ruta.getListaTrips();

			System.out.println("Tama�o lista trips: "+ listaTrips.getSize());

			for (int j = 0; j < listaTrips.getSize(); j++) {

				VOTrip trip = listaTrips.getElement(j);
				IList<VOStopTimes> listaStopTimes = trip.getListaStopTimes();
				//System.out.println("Tama�o lista Stop times" + listaStopTimes.getSize());

				for (int k = 0; k <  listaStopTimes.getSize();  k++) {

					VOStopTimes stopTime = listaStopTimes.getElement(k);
					//System.out.println("Entra al ultimo for" + stopTime.getStopId());

					if(stopTime.getStopId() == stopId )
					{
						System.out.println("Entra a if agregar a la lista");
						Nodo <VORoute> n = new Nodo<VORoute>(ruta, null, null);
						rutasEnParadas.add(n);


					}


				}


			}

		}


		return rutasEnParadas;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {


		
		
		
		return null;
	}

}
